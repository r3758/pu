<?php

include(dirname(__FILE__) . "/../library/database.php");
class Articles extends Database
{
    protected $db;

    public function __construct()
    {
        $this->db = parent::__construct();
    }

    public function create($CreateTitle, $CreateContent, $user_id, $time)
    {
        $statement = $this->db->prepare("INSERT INTO `articles`(`title`, `content`, `user_id`, `created_at`) VALUES (?, ?, ?, ?)");
        $statement->execute([$CreateTitle, $CreateContent, $user_id, $time]);
    }

    public function getAllcomments()
    {
        $statment = $this->db->prepare("SELECT id, name, title, content, created_at, updated_at FROM articles");
        $statment-> execute();

        return $statment->fetchAll(PDO::FETCH_ASSOC);
    }

    public function updateArticle($id, $title, $content)
    {
        $statement = $this->db->prepare("UPDATE articles SET `title` = ?, `content` = ?, `updated_at` = current_timestamp() WHERE `id` = ?");
        $statement->execute([$title, $content, $id]);

        return $statement;
    }
    public function deleteArticle($id)
    {
        $statement = $this->db->prepare("DELETE FROM messageboard WHERE id= ?");
        $statement->execute([$id]);

        return $statement;
    }
}
