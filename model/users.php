<?php

include(dirname(__FILE__) . "/../library/database.php");
class Users extends Database
{
    protected $db;

    public function __construct()
    {
        $this -> db = parent::__construct();
    }

    public function getUser($name)
    {
        $query = $this->db->prepare("SELECT `id`, `password` FROM `users` WHERE `name` = ?");
        $query->execute([$name, ]);
        $query = $query->fetch(PDO::FETCH_ASSOC);

        return $query;
    }

    public function registerAccount($name, $email, $password)
    {
        $statement = $this->db->prepare("INSERT INTO users(name, email, password) VALUES(?, ?, ?)");
        $statement->execute([$name, $email, $password]);

        return $statement;
    }

    public function registerAccountCheck($name, $email)
    {
        $statement = $this->db->prepare("SELECT name, email FROM users Where name = ?, email = ?");
        $statement->execute([$name, $email]);

        return $statement;
    }
}
