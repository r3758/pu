<?php

session_start();
include(dirname(__FILE__) . "/../../model/articles.php");

class Edit
{
    public function edit($id, $title, $content)
    {
        $articlesModel = new Articles;
        $result = $articlesModel->updateArticle($id, $title, $content);

        return $result;
    }
}

$id = $_POST['id'];
$title = $_POST["editTitle"];
$content = $_POST["editContent"];
$edit = new Edit;
$edit->edit($id, $title, $content);

Header('Location: ../../article/list.php');
