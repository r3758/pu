<?php

include(dirname(__FILE__) . "/../../model/articles.php");

class Delete
{
    public function delete($id)
    {
        $articleModel = new Articles();
        $result = $articleModel->deleteArticle($id);

        return $result;
    }
}

$id = $_POST['id'];
$delete = new Delete();
$delete->delete($id);
header('Location:../../article/list.php');
