<?php

include(dirname(__FILE__) . "/../../model/articles.php");
session_start();
date_default_timezone_set('Asia/Taipei');
class Create
{
    public function create($CreateTitle, $CreateContent, $userId, $time)
    {
        $article = new Articles();
        $article->create($CreateTitle, $CreateContent, $userId, $time);
    }
}

$CreateTitle = $_POST["CreateTitle"];
$CreateContent = $_POST["CreateContent"];
if (!$CreateTitle == null || !$CreateContent == null) {
    $userId = $_SESSION["id"];
    $time = date("Y-m-d H:i:s");
    $create = new Create();
    $create->create($CreateTitle, $CreateContent, $userId, $time);
}
header("location: ../../article/list.php");
