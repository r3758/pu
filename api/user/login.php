<?php

include(dirname(__FILE__) . "/../../model/users.php");
session_start();
class Login
{
    public function login($userName)
    {
        $getUser = new("Users");
        $userData = $getUser->getUser($userName);

        return $userData;
    }
}
$userName = $_POST["UserName"];
$password = $_POST["Password"];
$login = new Login();
$userData = $login->login($userName);
if (password_verify($password, $userData["password"])) {
    $_SESSION["id"] = $userData["id"];
    header("location: ../../article/list.php");
} else {
    header("location: ../../user/login.php");
}
