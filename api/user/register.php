<?php

include(dirname(__FILE__) . "../../model/users.php");

class Register
{
    public function register($name, $email, $password)
    {
        $articleModel = new Users();
        if (!$articleModel->registerAccountCheck($name, $email)) {
            return ;
        }
        $encryptedPassword = password_hash($password, PASSWORD_DEFAULT);
        $result = $articleModel->registerAccount($name, $email, $encryptedPassword);

        return $result;
    }
}

$name = $_POST['UserName'];
$email = $_POST['mail'];
$password = $_POST['Password'];
$re_password = $_POST['rePassword'];
$register = new Register();
if ($re_password == $password) {
    $register->register($name, $email, $password);
}
header('Location:../../user/register.php');
