<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../assets/css/register.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <title>留言版</title>
</head>

<body>
    <div class="container-fluid test">
        <div id="bg"></div>
        <div class="register">
            <div class="registBoard">
                <form action="../api/user/register.php" method="post" class="inputPlace">
                    <h2 class="registText">註冊</h2>
                    <div class="mail">
                        <span class="registText2">電子信箱</span>
                        <input required type="text" placeholder="請輸入電子信箱" name="mail" id="mail" style="font-size:20px">
                    </div>
                    <div class="userName">
                        <span class="registText2">帳戶名稱</span>
                        <input required type="text" placeholder="請輸入帳戶名稱" name="UserName" id="userName" style="font-size:20px">
                    </div>
                    <div class="password">
                        <span class="registText2">設置密碼</span>

                        <input required type="password" placeholder="需包含英文+數字" name="Password" id="password" style="font-size:20px">
                    </div>
                    <div class="rePassword">
                        <span class="registText2">確認密碼</span>

                        <input required type="password" placeholder="再次輸入密碼" name="rePassword" id="rePassword" style="font-size:20px">
                    </div>
                    <div class="submit">
                        <input type="submit" value="立即註冊" id="submitBtn" name="submit">
                    </div>
                </form>
                <a href="./login.php">
                    <button class="haveAccount btn btn-secondary">返回登入</button>
                </a>
            </div>
        </div>
    </div>
</body>

</html>
