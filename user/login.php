<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../assets/css/login.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <title>留言版</title>
</head>

<body>
    <div class="container-fluid test">
        <div id="bg"></div>
        <div class="login">
            <div class="loginBoard">
                <form action="../api/user/login.php" method="post" class="inputPlace" name="send" onsubmit="return chk();">
                    <h2 class="loginText">登入</h2>
                    <div class="userName">
                        <span class="loginText2">帳戶名稱</span>
                        <input required type="text" placeholder="請輸入帳戶名稱" name="UserName" id="userName" style="font-size:20px">
                    </div>
                    <div class="password">
                        <span class="loginText2">輸入密碼</span>
                        <input required type="password" placeholder="請輸入密碼" name="Password" id="password" style="font-size:20px">
                    </div>
                    <div class="submit">
                        <input type="submit" value="立即登入" id="submitBtn" name="submit">
                    </div>
                </form>
                <a class="noAccount" href="./register.php">註冊一個帳號</a>
            </div>
        </div>
    </div>
</body>

</html>
