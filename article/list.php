<?php
session_start();
include(dirname(__FILE__) . "\..\api\article\list.php");
$List = new Show();
$statement = $List->list();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="../assets/css/article.css">
    <title>列出文章</title>
</head>

<body>
    <?php
    if (!$_SESSION['id'] == "") {
    ?>
        <a href="./create.php" class="article-add">
            <button type="button"> + </button>
        </a>
    <?php
    } ?>


    <header style="position: fixed; top: 0; width:100%; z-index: 2">
        <nav class="navbar navbar-expand-sm navbar-light bg-light">
            <div class="container-fluid">
                <a class="navbar-brand" href="#">留言板</a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ms-auto  mb-lg-0">
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="page" href="#">文章列表</a>
                        </li>
                        <?php
                        if ($_SESSION['id'] == "") {
                        ?>
                            <li class="nav-item">
                                <a class="nav-link" href="../user/login.php">登入</a>
                            </li>
                        <?php
                        } else { ?>
                            <li class="nav-item">
                                <a class="nav-link" href="#">登出</a>
                            </li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
        </nav>

    </header>

    <div class="container article-list">
        <h2>文章列表</h2>
        <?php
        foreach ($statement as $row) { ?>
            <div class="row mt-5 card card-body">
                <div class="col-12">
                    <span><?php echo $row['created_at'] ?></span>
                    <span><?php echo $row['name'] ?></span>
                    <h2><?php echo $row['title'] ?></h2>
                    <span>
                        <?php echo $row['content'] ?>
                    </span>
                    <form action="../api/article/delete.php">
                        <input type="hidden" value="<?php echo $row['id'] ?>" name="id">
                        <button class="btn btn-danger" type="submit">刪除</button>
                    </form>
                    <form action="../api/article/edit.php">
                        <input type="hidden" value="<?php echo $row['id'] ?>" name="id">
                        <button class="btn btn-primary" type="submit">編輯</button>
                    </form>
                </div>
                <hr class="mt-3">
                <div>
                    <a href="../comment/create.php"><button class="btn btn-secondary">查看留言</button></a>
                </div>
            </div>
        <?php }
        ?>
    </div>

</body>

</html>
